# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/carlpett/sops" {
  version     = "0.6.2"
  constraints = "0.6.2"
  hashes = [
    "h1:xeriMBVFQS4OFEATYnnmghy4lqQe2yetynkWcdsx/Is=",
    "zh:51e101c9a6d403300f2bd552cfbbb6b3798433f9cc03c46f60508a431c724af4",
    "zh:805fbbb5a5b9bb7bdc205e389d754b71d4b0f691f15a634bc73fad432fb4cda9",
    "zh:abd78a2a0702dd288217c9a1b9d327258621e1fc20bea9896760e42649edf521",
    "zh:c1f7958037d024f03f4948b0b20df36c949dbd89768fcbdfd86e419394412528",
    "zh:f2ed0414923de68de6d8aa2ceb0ba867b9373f93f572fd4fac8ea906258ccb6f",
  ]
}

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "3.3.0"
  constraints = "3.3.0"
  hashes = [
    "h1:5KU99XGEVhXTgERCVFoL/p61WCsMJOWhG52tqePzaCM=",
    "h1:7BiyNB1tcWfqMTpY6fBbDH0BzIbNiW9/ZiXRbjiJw2c=",
    "h1:Er8bgWCEmRSJyIGikF2K95KB8UavDGykWcjoT1+oUT0=",
    "h1:FyeWDcVUnkdFrCOz+Sty17HfEaiREDJMJpynD3WtlUc=",
    "h1:Hc3zmK1v8UgtaBR2j+rcLymHX35fP4v7XrHi4keTn4o=",
    "h1:J9oWWkNeNSWMrL8y+XyFFD5rstiB8HBXe8DGIEST8J0=",
    "h1:MeC5/NO50TciLmT56MnQA6nmRVMDm09IqczAo+UJvjo=",
    "h1:NE6RYH95TaE8RMFFgPgtwpVTuzO67FhI55hHU6G/RBY=",
    "h1:PSq5mYZsPex2878DlXAEzF9zhJvzONHxtYxBk3n3gio=",
    "h1:eW/uQfpZBHm4rkKYWpqbSNhFawCKOMDdnEQK2dspyX0=",
    "h1:lbUbPKYyoPVhek60/iV8130isdSPEnt1xnj+JaJsrTI=",
    "h1:pvCJdEtxUl5P3+qoKQr/DSpx2LREdhdZknVnY/p1g60=",
    "h1:uyNwRCqGq5ktKlJ5Yyeb+18chmbK+MDeIWYAIJpzpyg=",
    "h1:wA+2ElP0WCm4Xxj4y1ehkFYwpACoui0b4q9hLoowst0=",
  ]
}
