# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/carlpett/sops" {
  version     = "0.6.3"
  constraints = "0.6.3"
  hashes = [
    "h1:1Dx43rBcTvC7sKDvAsLwM038mph2BBLs9VcF4UAGs7A=",
    "zh:0c9f0cad271cbd4c3fa97961b6276ff86f7db2d5dc39ea7de687b5e5091b5f4b",
    "zh:4180cf700dc8ccc71db5d4a0496c22a54301a617ae53d93bba91cb142694552e",
    "zh:656f35d78120bd50d82767a814f53a0a4f96ff1f5f79e40089f259bb1f09ce9e",
    "zh:98e53430bbf13631b314f9107372262151503c133b0551a370e23a0b451f5005",
    "zh:a9c5cdcf12ea89eee54e55779f0e0c868350fc2b48b79a394500d3b04ea28919",
    "zh:b042e80e60c0745ee3c8c49860ecb6e069f75c4c60aa8d6dd2f188f8c0a4f4ab",
    "zh:fe2194ecf065beb9a384b4893cd9d3d975e39db89b38d2b5af05c43352d83397",
  ]
}

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "3.3.0"
  constraints = "3.3.0"
  hashes = [
    "h1:5KU99XGEVhXTgERCVFoL/p61WCsMJOWhG52tqePzaCM=",
    "h1:7BiyNB1tcWfqMTpY6fBbDH0BzIbNiW9/ZiXRbjiJw2c=",
    "h1:Er8bgWCEmRSJyIGikF2K95KB8UavDGykWcjoT1+oUT0=",
    "h1:FyeWDcVUnkdFrCOz+Sty17HfEaiREDJMJpynD3WtlUc=",
    "h1:Hc3zmK1v8UgtaBR2j+rcLymHX35fP4v7XrHi4keTn4o=",
    "h1:J9oWWkNeNSWMrL8y+XyFFD5rstiB8HBXe8DGIEST8J0=",
    "h1:MeC5/NO50TciLmT56MnQA6nmRVMDm09IqczAo+UJvjo=",
    "h1:NE6RYH95TaE8RMFFgPgtwpVTuzO67FhI55hHU6G/RBY=",
    "h1:PSq5mYZsPex2878DlXAEzF9zhJvzONHxtYxBk3n3gio=",
    "h1:eW/uQfpZBHm4rkKYWpqbSNhFawCKOMDdnEQK2dspyX0=",
    "h1:lbUbPKYyoPVhek60/iV8130isdSPEnt1xnj+JaJsrTI=",
    "h1:pvCJdEtxUl5P3+qoKQr/DSpx2LREdhdZknVnY/p1g60=",
    "h1:uyNwRCqGq5ktKlJ5Yyeb+18chmbK+MDeIWYAIJpzpyg=",
    "h1:wA+2ElP0WCm4Xxj4y1ehkFYwpACoui0b4q9hLoowst0=",
  ]
}

provider "registry.terraform.io/dmacvicar/libvirt" {
  version     = "0.6.11"
  constraints = "0.6.11"
  hashes = [
    "h1:/Z7k0YIRyD64rgjpjOjodOitWE3ItxHbP9ng6da8XZY=",
    "h1:6QzHY/7aNdaaDxJZKygotWnM5uHoS2gs/03CzUCJX60=",
    "h1:Axxa+XFEIbdtGWK11Z/50P+kC7K3EtHRGZ70FgP3kWY=",
    "h1:BWUChZkfaZBrJBxuuPYkBbOS1bLFchx0EhoX0U91mhc=",
    "h1:BxZ7AMP5wNO9/z+WS6BB+f8qDqW2k/1FJkfOCzEJgRg=",
    "h1:Fs8eA5JRLGyNq5Fa3CcDEobmVQ+g5VCamoaRwaOwBOg=",
    "h1:H2i7US9Nc4C8ybnP0XsYViwuLXoOErqebLX+dRLUvQQ=",
    "h1:JdbyHYvOo8lGs55EGfL7X1d09rQXXu4n69YnklXi9FU=",
    "h1:RPOMABDll63qLw/NF5+S0RxCEFOy9mkGsD/geImfitA=",
    "h1:VQMHeiT9Cfbea/CSXsFssY401zNaMBAuRCMKWC5qNyA=",
    "h1:r+xMYDufQcGzdXis/63n9bBV9yB/GQBAlOOrcaJM7Cs=",
    "h1:tqT5NJpHHXP8GtQ7x1ckCVDZp2/aymE8ToFkNoL20ZM=",
    "h1:w8PraFsu82FhtiYxXlOYea0KRvslBGgxdXuMiLknC0c=",
  ]
}

provider "registry.terraform.io/poseidon/ct" {
  version     = "0.9.1"
  constraints = "0.9.1"
  hashes = [
    "h1:5eLxHZTRorwY/7QCfXelFR7FIxLs+GuHr7prAUBilyM=",
    "h1:LWMX1hhWX97slT1xwxomQp6V4MMem3fc3yoUWro97rY=",
    "h1:P4k4FJRY25ROC+mib4e9hRmcwsMy0yn/p3s/xosdOes=",
    "h1:mNrWYeIjoCGIC3Wl4cKjAsY969beae5/yYUI87IUPJ0=",
    "h1:xqhXddg3sQAgnNQw7ld32pdOAUaPc0WJZsK6IOo/F8c=",
  ]
}
