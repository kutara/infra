# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/digitalocean/digitalocean" {
  version     = "2.14.0"
  constraints = "2.14.0"
  hashes = [
    "h1:/r28GjpyiGzab7mMTmO6v4SMSb4yB8QdY2ueGFf51ZU=",
    "h1:1vUV8CtOfONN5TxeNd88Xl6aRhT7gmi9IoHKq1j1MvM=",
    "h1:6NjDp8oLAoPkM+WwKTeGDFsCiJ3T41+yQk3/JWw/JPY=",
    "h1:7SU32IBOWHvF80U2Mzy7xKMOz5JZwgijJvlS95CLxPk=",
    "h1:DRKnhUa/NUQ1DBXD9I4GT40xQjyy2/fPocqMYId9eSE=",
    "h1:FjP/xW0/aNpZ/4hoUMlkd9fmggOth1UPB0YRILQzcZY=",
    "h1:PbgdY2aWLkhmbd9WVZSRsGepBCMvsjqSyinhzQ7j0Z8=",
    "h1:R36nIWX3tnxpkOOR4Y8xHIHMwORdwjlj1sDFWpvatXE=",
    "h1:SjrlJJKuUHLeNj0OpW3l85kUDVWrSriiGmveiDQd3Pk=",
    "h1:TrWLO8XQVNkNRsEJV8S7MpPGjINqF98C9PHbyFsxTwY=",
    "h1:VgOC/Y3OIYezumhYMYhETZyQWjYLzDZwHBNRSezuf90=",
    "h1:Yo5ZIWbNGrgbrsCDxl8YS3Ra9GFsTY6IULNdlC347qE=",
    "h1:Yvwvzw7umEcOEtCospWF7gZ7hh1iVM/QUfIyskukBEo=",
    "h1:mweBY6X5RubSqiJwqj8m1gURO4NB1Wgfaihy1TwWqpY=",
    "h1:tCaHDQxou73zrhlCOhZeHeTwO9jzFcooSot/og1G7D0=",
  ]
}
